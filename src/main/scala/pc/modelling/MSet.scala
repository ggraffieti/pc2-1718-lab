package pc.modelling

trait MSet[A] extends (A => Int) {
  def union(m: MSet[A]): MSet[A]
  def diff(m: MSet[A]): MSet[A]
  def disjoined(m: MSet[A]): Boolean
  def size: Int
  def matches(m: MSet[A]): Boolean
  def extract(m: MSet[A]): Option[MSet[A]]
  def asList: List[A]
  def asMap: Map[A,Int]
  def iterator: Iterator[A]
  def matchIterator: Iterator[(A,MSet[A])]
}

object MSet {
  // Factories
  def apply[A](l: A*): MSet[A] = new MSetImpl(l.toList)
  def ofList[A](l: List[A]): MSet[A] = new MSetImpl(l)
  def ofMap[A](m: Map[A,Int]): MSet[A] = new MSetImpl(m)

  // Hidden reference implementation
  private case class MSetImpl[A](val asMap: Map[A,Int]) extends MSet[A] {

    // keeping both representations, as Map and as List
    override val asList: List[A] = asMap.toList.flatMap{case (a,n) => List.fill(n)(a)}

    def this(list: List[A]) = this(list.groupBy(a=>a).map{case (a,n) => (a,n.size)})

    override def apply(v1: A): Int = asMap(v1)
    override def union(m: MSet[A]): MSet[A] = new MSetImpl[A](asList ++ m.asList)
    override def diff(m: MSet[A]): MSet[A] = new MSetImpl[A](asList diff m.asList)
    override def disjoined(m: MSet[A]): Boolean = (asList intersect m.asList).isEmpty
    override def size(): Int = asList.size
    override def matches(m: MSet[A]): Boolean = extract(m).isDefined
    override def extract(m: MSet[A]): Option[MSet[A]] = Some(this diff m) filter (_.size == size - m.size)
    override def iterator(): Iterator[A] = asList.distinct.iterator
    override def matchIterator: Iterator[(A,MSet[A])] = iterator().map(a => (a,extract(MSet(a)).get))
    override def toString = s"{${asList.mkString("|")}}"
  }

  // Syntactic sugar for creating out of any product, e.g., tuple
  implicit def FromProduct[A](p: Product): MSet[A] =
    MSet.ofList(p.productIterator.toList.asInstanceOf[List[A]])
}

object TryMSet extends App {
  import MSet._

  val m1 = MSet(10,20,30,30,40,40,50)
  val m2: MSet[Int] = (50,10,20,30,30,40,40)
  println(m1)
  println(m1 == m2)
  println(m1.asList)
  println(m1.asMap)
  println(m1.iterator.toList)
  println(m1.matchIterator.toList)
  println(m1.extract(MSet(50,10)))
}
